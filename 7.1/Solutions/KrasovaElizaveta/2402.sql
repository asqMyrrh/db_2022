drop table if exists Tour cascade;
drop table if exists City cascade;
drop table if exists Review cascade;
drop table if exists "CityToTour" cascade;

create table Tour
(
	name varchar(128)
		constraint "PK_name_tour" primary key,
	price numeric(15, 6)
);

create table City
(
	name varchar(128)
		constraint "PK_name_city" primary key,
	founded text
);

create table Review
(
	id bigint generated always as identity
        constraint "PK_id" primary key,
	comment text,
	ratio integer not null,
	tour varchar(128)
		constraint "FK_tour_review" references tour not null
);

create table "CityToTour"
(
	"NameTour" varchar(128) not null,
	"NameCity" varchar(128) not null,
		constraint "FK_nameCity_toCity" foreign key("NameCity") references City,
		constraint "FK_nameTour_toTour" foreign key("NameTour") references Tour
);

alter table "CityToTour"
	add constraint "UQ_CityToTour" unique("NameTour", "NameCity");

insert into Tour
values  ('В гостях у Хаски', 5000),
        ('Хиты карелии', 15000);

insert into City
values  ('Приозерск', '1295'),
        ('Сортавала', '1468'),
        ('Валаам', '1407');

insert into "CityToTour"
values  ('В гостях у Хаски', 'Приозерск'),
        ('В гостях у Хаски', 'Сортавала'),
        ('Хиты карелии', 'Сортавала'),
        ('Хиты карелии', 'Валаам');

insert into Review(comment, ratio, tour)
values  ('Отличный тур', 5, 'Хиты карелии'),
        ('Не понравилось', 2, 'Хиты карелии');

select "NameTour", "NameCity", comment, ratio
from Tour
	join "CityToTour" on Tour.name = "CityToTour"."NameTour"
	join City on City.name = "CityToTour"."NameCity"
	join Review on Tour.name = Review.tour

