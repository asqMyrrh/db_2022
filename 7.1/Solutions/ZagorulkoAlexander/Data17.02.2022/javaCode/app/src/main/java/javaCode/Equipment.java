package javaCode;

public class Equipment {
    String title;
    int count;
    Holder holder;

    public Equipment(String title, int count, Holder holder){
        this.title = title;
        this.count = count;
        this.holder = holder;
    }

    public String getTitle(){
        return this.title;
    }
}
