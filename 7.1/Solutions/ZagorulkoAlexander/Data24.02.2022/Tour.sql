drop table if exists Tour cascade;
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists "CityToTour" cascade;

Create table Tour
(
    name varchar(128)
        constraint "PK_name_tour" primary key,
    price numeric(15, 6)
);

Create table City
(
    name varchar(128)
        constraint "PK_name_city" primary key,
        founded date
);

Create table Review
(
    id bigint generated always as identity
        constraint "PK_id_review" primary key,
    comment text,
    ratio integer not null,
    tour varchar(128)
		constraint "FK_tour_review" references tour not null
);

create table "CityToTour"
(
    "TourName" varchar(128) not null,
    "CityName" varchar(128) not null,
        constraint "FK_cityName_toCity" foreign key("CityName") references City,
        constraint "FK_tourName_toTour" foreign key("TourName") references Tour
);

alter table "CityToTour"
	add constraint "UQ_CityToTourr" unique("TourName", "CityName");

insert into Tour
values  ('В гостях у Хаски', 12500),
        ('Хиты карелии', 25000);

insert into City
values  ('Приозерск', '1295-01-01'),
        ('Сортавала', '1468-01-01'),
        ('Валаам', '1407-01-01');

insert into Review(comment, ratio, tour)
values  ('Отличный тур', 5, 'Хиты карелии'),
        ('Не понравилось', 2, 'Хиты карелии');

insert into "CityToTour"
values  ('В гостях у Хаски', 'Приозерск'),
        ('В гостях у Хаски', 'Сортавала'),
        ('Хиты карелии', 'Сортавала'),
        ('Хиты карелии', 'Валаам');

select "TourName", price, comment, ratio
from Tour
	join "CityToTour" on Tour.name = "CityToTour"."TourName"
	join Review on Tour.name = Review.tour
