drop table if exists tour cascade;
drop table if exists review cascade;

create table tour
(
    name  text constraint "PK_name" primary key,
    price int
);

create table review
(
    comment text,
    ratio int,
    "tourName" text
        constraint "FK_tourName" references tour
);

insert into tour
values  ('Гавайи', 1000),
		('Мальдивы', 1500),
		('Доминиканы', 2000);

insert into review (comment, ratio, "tourName")
values  ('Отлично!', 7, 'Гавайи'),
		('на Гавайях было тепло и хорошо!', 8, 'Гавайи' ),
		('Супер!', 9, 'Мальдивы'),
		('Не пожалел, что приехал на Мальдивы!', 10, 'Мальдивы'),
		('Хорошо!', 10, 'Доминиканы'),
		('На Гавайях мне понравилось больше, чем на Доминиканах!', 5, 'Доминиканы');

select name, price, comment as "reviewComment", ratio as "reviewRatio"
from tour
         join review on name = "tourName";