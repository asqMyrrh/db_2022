import psycopg2
from psycopg2.extras import RealDictCursor


class Database:
    def __init__(self):
        self.conn = psycopg2.connect(database="postgres", user='postgres',
                                     password='changeme', host='localhost', port=999, cursor_factory=RealDictCursor)

    def select_tour(self):
        cursor = self.conn.cursor()
        cursor.execute("select * from tour;")
        self.conn.commit()
        return cursor.fetchall()

    def select_review(self):
        cursor = self.conn.cursor()
        cursor.execute("select * from review;")
        self.conn.commit()
        return cursor.fetchall()


class Tour:
    def __init__(self, name, price):
        self.name = name
        self.price = price
        self.reviews = []

    def setReview(self, review):
        self.reviews.append(review)

    def printTour(self):
        print("тур: ", self.name)
        print("цена: ", self.price)
        print("отзывы:")
        for elem in self.reviews:
            print(elem.comment, 'оценка', elem.ratio)

class Review:
    def __init__(self, comment, ratio, tour):
        self.comment = comment
        self.ratio = ratio
        self.tour = tour

    def printReview(self):
        print("Имя тура: ", self.tour.name)
        print("Оценка: ", self.ratio)
        print("Комментарий: ", self.comment)

db = Database()
tours1 = db.select_tour()
reviews1 = db.select_review()
tours2 = [] #Здесь хранятся экземпляры класса Tour
for tour in tours1:
    tours2.append(Tour(dict(tour)["name"], dict(tour)["price"]))

reviews2 = [] #Здесь хранятся экземпляры класса Review
for review in reviews1:
    for tour in tours2:
        if tour.name == dict(review)["tourName"]:
            reviews2.append(Review(dict(review)["comment"], dict(review)["ratio"], tour))
            break

for tour in tours2:
    for review in reviews2:
        if tour == review.tour:
            tour.setReview(review)

def selectJoin():
    for tour in tours2:
        tour.printTour()

selectJoin()

