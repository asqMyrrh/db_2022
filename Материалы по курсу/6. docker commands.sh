﻿docker run --name my-postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=changeme -e PGDATA=/pgdata -v /home/sirius/Documents/docker/postgresql_db2022/pgdata:/pgdata -p 999:5432 -d postgres

docker ps # список запущенных контейнеров
docker ps -a # список всех контейнеров

docker start container_name # запуск контейнера
docker stop container_name # остановка контейнера
docker rm container_name # удаление контенера
