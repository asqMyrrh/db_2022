drop table if exists "User" cascade ;
drop table if exists "Settings" cascade ;

create table "User"
(
    nickname text,
    first_name text,
    last_name text
);
create table "Settings"
(
    front_size int,
    color_theme text,
    user_name text
);

alter table "User"
    add constraint "PK_settings" primary key (nickname);

alter table "Settings"
    add constraint "Fk_userame" foreign key (user_name) references "User" (nickname);
    
alter table "Settings"
    add unique (user_name);

insert into "User"(nickname, first_name, last_name)
values ('robert','rob','stryzhak'),
       ('bob4insk','arsen','arsov');

insert into "Settings"(front_size, color_theme, "user_name")
values (12,'blue','robert'),
       (14,'black','bob4insk');

select *
from "User"
    join "Settings" on nickname = user_name;
