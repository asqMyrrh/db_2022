drop table if exists users cascade;
drop table if exists settings cascade;

create table users
(
    nickname  text,
    first_name text,
    last_name text

);


create table Settings
(
    font_size  int,
    color_scheme text,
    users_nickname text

);

alter table users
    add primary key (nickname);

alter table Settings
    add foreign key (users_nickname) references users (nickname);

alter table Settings
    add unique (users_nickname);


insert into users
	values  ('laverati', 'Elizabeth', 'Mamrenko'),
	        ('eropuk21', 'Egor', 'Hlapov');

insert into Settings
	values  (18, 'orange', 'laverati'),
	        (19, 'black', 'eropuk21');

select nickname, first_name, last_name, font_size, color_scheme
from users
         join Settings  on nickname = users_nickname;

