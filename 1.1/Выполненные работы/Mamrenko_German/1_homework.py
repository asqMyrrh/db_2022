from typing import List
import psycopg2
from itertools import groupby
from psycopg2.extras import RealDictCursor

conn = psycopg2.connect("dbname=postgres user=postgres password=changeme host=localhost port=999",
                        cursor_factory=RealDictCursor)


def one_to_many():
    class Holder:
        def __init__(self, name: str, phone: str):
            self.name = name
            self.phone = phone
            self.equipment: List[Equipment] = []

    class Equipment:
        def __init__(self, title: str, count: int, holder: Holder):
            self.title = title
            self.count = count
            self.holder = holder

    cur = conn.cursor()
    query = """
    select name as "holderName", phone as "holderPhone",\
    title as "equipmentTitle", count as "equipmentCount"
    from holder 
        join equipment on phone = "holderPhone";
    """

    cur.execute(query)
    result = cur.fetchall()
    print(result)
    result.sort(key=lambda row: row['holderPhone'])
    holders = []
    for key, group in groupby(result, lambda row: row['holderPhone']):
        entries = list(group)
        holder = Holder(entries[0]['holderName'], entries[0]['holderPhone'])
        holders.append(holder)
        for entry in entries:
            equipment = Equipment(
                entry['equipmentTitle'], entry['equipmentCount'], holder)
            holder.equipment.append(equipment)
    return holders


one_to_many()
